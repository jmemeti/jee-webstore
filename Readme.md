= add user jso (password: camp2016) =
jso@jsoVM:~/jboss/wildfly-10.0.0.Final/bin$ ./add-user.sh 

What type of user do you wish to add? 
 a) Management User (mgmt-users.properties) 
 b) Application User (application-users.properties)
(a): b

Enter the details of the new user to add.
Using realm 'ApplicationRealm' as discovered from the existing property files.
Username : jso
Password recommendations are listed below. To modify these restrictions edit the add-user.properties configuration file.
 - The password should be different from the username
 - The password should not be one of the following restricted values {root, admin, administrator}
 - The password should contain at least 8 characters, 1 alphabetic character(s), 1 digit(s), 1 non-alphanumeric symbol(s)
Password : 
WFLYDM0102: Password should have at least 1 non-alphanumeric symbol.
Are you sure you want to use the password entered yes/no? yes
Re-enter Password : 
What groups do you want this user to belong to? (Please enter a comma separated list, or leave blank for none)[  ]: User 
About to add user 'jso' for realm 'ApplicationRealm'
Is this correct yes/no? yes
Added user 'jso' to file '/home/jso/jboss/wildfly-10.0.0.Final/standalone/configuration/application-users.properties'
Added user 'jso' to file '/home/jso/jboss/wildfly-10.0.0.Final/domain/configuration/application-users.properties'
Added user 'jso' with groups User to file '/home/jso/jboss/wildfly-10.0.0.Final/standalone/configuration/application-roles.properties'
Added user 'jso' with groups User to file '/home/jso/jboss/wildfly-10.0.0.Final/domain/configuration/application-roles.properties'
Is this new user going to be used for one AS process to connect to another AS process? 
e.g. for a slave host controller connecting to the master or for a Remoting connection for server to server EJB calls.
yes/no? no

= add queue =
In JBoss Management interface, go to Subsystem: Messaging - ActiveMQ    Messaging Provider: default

Add the following queue
Name: orderQueue
JNDI Names: java:/queue/order
Durable?: true
