package com.jee.webstore.orderprocessing.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "ORDERS")
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false)
	private String externalIds;
	@Column(nullable = false)
	private String customerUsername;
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getExternalIds() {
		return externalIds;
	}

	public void setExternalIds(String externalIds) {
		this.externalIds = externalIds;
	}

	public String getCustomerUsername() {
		return customerUsername;
	}

	public void setCustomerUsername(String customerUsername) {
		this.customerUsername = customerUsername;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setExternalIds(List<String> externalIds) {
		String ids = externalIds.stream()
				.collect(Collectors.joining(","));
		this.externalIds = ids;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", externalIds=" + externalIds + ", customerUsername=" + customerUsername
				+ ", status=" + status + "]";
	}
}