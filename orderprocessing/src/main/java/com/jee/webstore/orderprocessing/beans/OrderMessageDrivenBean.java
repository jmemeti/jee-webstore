package com.jee.webstore.orderprocessing.beans;

import com.jee.webstore.common.message.OrderDto;
import com.jee.webstore.orderprocessing.persistence.Order;
import com.jee.webstore.orderprocessing.persistence.Status;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.util.logging.Logger;

@MessageDriven(mappedName = "jms/order", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/queue/order") })
public class OrderMessageDrivenBean implements MessageListener {

	private final static Logger logger = Logger.getLogger(OrderMessageDrivenBean.class.getName());

	@EJB
	private OrderService orderService;

	@Override
	public void onMessage(Message incoming) {
		try {
			if (incoming instanceof ObjectMessage) {
				ObjectMessage ObjectMessage = (ObjectMessage) incoming;
				OrderDto orderDto = (OrderDto) ObjectMessage.getObject();

				logger.info("got new incoming message: " + orderDto);

				Order order = mapToOrder(orderDto);
				orderService.addOrder(order);
			}
		} catch (JMSException e) {
			logger.severe("error during processing of message");
		}
	}

	private Order mapToOrder(OrderDto orderDto) {
		Order order = new Order();
		order.setExternalIds(orderDto.getExternalIds());
		order.setCustomerUsername(orderDto.getCustomerUsername());
		order.setStatus(Status.OPEN);
		return order;
	}

}
