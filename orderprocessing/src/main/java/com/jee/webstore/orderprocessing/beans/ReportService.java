package com.jee.webstore.orderprocessing.beans;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;

import com.jee.webstore.orderprocessing.dao.OrderDao;
import com.jee.webstore.orderprocessing.persistence.Order;

@LocalBean
@Singleton
public class ReportService {

	private Logger logger = Logger.getLogger(ReportService.class.getName());
	
	@Inject
	private OrderDao orderDao;
	
	@Schedule(second = "*/10", minute="*",hour="*", persistent = false)
	public void printReport(Timer timer) {
		List<Order> orders = orderDao.findAll();
		
		StringBuffer output = new StringBuffer("Report:");
		orders.forEach(o -> output.append("\n" + o.toString()));
		
		logger.info(output.toString());
	}
	
}
