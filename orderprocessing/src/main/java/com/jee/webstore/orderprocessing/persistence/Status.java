package com.jee.webstore.orderprocessing.persistence;

public enum Status {

	OPEN, PACKING, SHIPPING, SENT
	
}
