package com.jee.webstore.orderprocessing.beans;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jee.webstore.orderprocessing.dao.OrderDao;
import com.jee.webstore.orderprocessing.persistence.Order;

@Stateless
public class OrderService {

    @Inject
    private OrderDao orderDao;

    public void addOrder(Order order) {
    	orderDao.add(order);
    }

    public Order getOrder(long id) {
        return orderDao.get(id);
    }

	public List<Order> getOrders() {
		return orderDao.findAll();
	}
}
