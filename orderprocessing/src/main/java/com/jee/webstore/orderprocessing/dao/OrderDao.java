package com.jee.webstore.orderprocessing.dao;

import java.util.List;

import javax.enterprise.inject.Model;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.jee.webstore.orderprocessing.persistence.Order;

@Model
public class OrderDao {

	@PersistenceContext
	private EntityManager entityManager;

	public void add(Order order) {
		entityManager.persist(order);
	}

	public Order get(long id) {
		return entityManager.find(Order.class, id);
	}

	public List<Order> findAll() {
		return entityManager.createQuery("SELECT o FROM Order o", Order.class).getResultList();
	}
}
