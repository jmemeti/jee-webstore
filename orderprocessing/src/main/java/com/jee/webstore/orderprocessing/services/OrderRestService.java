package com.jee.webstore.orderprocessing.services;

import java.util.List;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.jee.webstore.orderprocessing.beans.OrderService;
import com.jee.webstore.orderprocessing.persistence.Order;

@Path("/order")
public class OrderRestService {

    @EJB
    private OrderService orderService;

    @GET
    @Path("/")
    @Produces("application/json")
    public Response getOrder() {
        List<Order> orders = orderService.getOrders();

        return Response.ok(orders).build();
    }
    
    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getOrder(@PathParam("id") long id) {
        Order order = orderService.getOrder(id);

        return Response.ok(order).build();
    }
    
}