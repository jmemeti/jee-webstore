package com.jee.webstore.common.message;

import java.io.Serializable;
import java.util.List;

public class OrderDto implements Serializable {

	private static final long serialVersionUID = -9019897839034281753L;

	private List<String> externalIds;
	private String customerUsername;

	public List<String> getExternalIds() {
		return externalIds;
	}

	public void setExternalIds(List<String> externalIds) {
		this.externalIds = externalIds;
	}

	public String getCustomerUsername() {
		return customerUsername;
	}

	public void setCustomerUsername(String customerUsername) {
		this.customerUsername = customerUsername;
	}

	@Override
	public String toString() {
		return "OrderDto [externalId=" + externalIds + ", customerUsername=" + customerUsername + "]";
	}
}
