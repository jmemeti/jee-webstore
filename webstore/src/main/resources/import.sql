insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (1, 'EpsonWorkforce', 'Epson WF-5620DWF WorkForce Pro', 222, 5, 'images/products/epson.jpg');
insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (2, 'Lumia950', 'Microsoft Lumia 950', 399, 42, 'images/products/lumia.jpg');
insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (3, 'HuawaiNexus6P', 'Huawei Nexus 6P', 599, 45, 'images/products/nexus6p.jpg');
insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (4, 'LogitechAnywhere2', 'Logitech MX Anywhere 2', 69, 22, 'images/products/logitechmx.jpg');
insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (5, 'LogitechComboMK520', 'Logitech Wireless Combo MK520', 49.9, 57, 'images/products/logitechkombo.jpg');
insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (6, 'Samsung850Evo', 'Samsung 850 EVO Basic', 166, 17, 'images/products/samsungevo.jpg');
insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (7, 'SamsungUE55JU6470', 'Samsung UE55JU6470', 799, 4, 'images/products/samsungtv.jpg');
insert into PRODUCTS (ID, EXT_ID, PRODUCT_NAME, PRICE, NUMBER_OF_AVAILABLE_ITEMS, IMG_URL) values (8, 'SamsungGearVR', 'Samsung Gear VR', 99, 54, 'images/products/samsunggear.jpg');
