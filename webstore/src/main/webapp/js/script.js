$(document).ready(function() {
    $('#spinner').show();

    $.ajax({
        type: "GET",
        url: "rest/products",
        success: function(data) {
            $('#spinner').hide();

            for (var i=0; i< data.length; i++) {
                var id = data[i].id;
                var name = data[i].name;
                var price = data[i].price;
                var imgUrl = data[i].imgUrl;
                var numberOfAvailableItems = data[i].numberOfAvailableItems;

                var detailsRowName = "detailsRow" + i;

                $('#productList > tbody:last-child').append(
                    '<tr class="clickable-row" onClick="showDetails(\x27'+detailsRowName+'\x27)">' +
                        '<th scope="row">'+id+'</th>' +
                        '<td>'+name+'</td>' +
                        '<td>'+price+' CHF</td>' +
                    '</tr>' +
                    '<tr name="'+detailsRowName+'" class="collapse out">' +
                        '<th scope="row"></th>' +
                        '<td><img src="' + imgUrl + '"></td>' +
                        '<td>' +
                            '<div class="grid" name="detailsGrid'+i+'">' +
                                '<div class="row">' +
                                    '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Available items: ' + numberOfAvailableItems + '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">' +
                                        '<button type="button" class="btn btn-default" aria-label="Left Align" onClick="addProductToCart('+id+','+i+')">' +
                                        '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add to Cart' +
                                        '</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</td>' +
                    '</tr>'
                );
            }
        }
    });
});

//var showDetails = function (id, index, imgUrl, numberOfAvailableItems) {
var showDetails = function (detailsRowClassName) {
    var detailsRow = $('tr[name="'+detailsRowClassName+'"]');

    if (detailsRow.hasClass("out")) {
        detailsRow.addClass("in");
        detailsRow.removeClass("out");
    } else {
        detailsRow.addClass("out");
        detailsRow.removeClass("in");
    }

    //todo hide other rows, if clicked on a different one?
};

var addProductToCart = function (productId, rowIndex) {
    $.ajax({
        type: "POST",
        url: "rest/cart?id="+productId,
        success: function(data) {
            //todo update available items!

            //todo avoid multiple ok icons when clicked multiple times

            $('div[name="detailsGrid'+rowIndex+'"]').append(
                '<div class="row">' +
                    '<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">' +
                        '<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color: rgb(0, 255, 0);"></span>' +
                    '</div>' +
                '</div>'
            );

            var badgeAmount = $('.badge').html();
            if (isNaN(badgeAmount)) {
                badgeAmount = 0;
            }
            badgeAmount = parseInt(badgeAmount) + 1;

            $('#shoppingCartLink').removeClass('disabled');
            $('#shoppingCartLink').html('<a href="#" onclick="order()">Buy Products in Shopping Cart <span class="badge">'+badgeAmount+'</span></a>');
        }
    });
};

var order = function () {
    $('#spinner').show();

    $.ajax({
        type: "POST",
        url: "rest/cart/checkout",
        success: function(data) {
            $('#spinner').hide();
            $('#shoppingCartLink').addClass('disabled');
            $('#shoppingCartLink').html('<a href="#">Buy Products in Shopping Cart</a></li>');

            $('.glyphicon-ok').remove();
        }, error: function(data) {
            alert("order did not work");
        }
    });
};