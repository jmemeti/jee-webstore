package com.jee.webstore.beans;

import com.jee.webstore.dao.ProductDao;
import com.jee.webstore.persistence.Product;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Stateful
@SessionScoped
public class CartBean implements Serializable {

    @EJB
    private ProductDao productDao;
    @EJB
    private ProductOrderService productOrderService;

    private List<Product> products = new ArrayList<>();

    public void addProduct(Product product) {
        products.add(product);
    }

    public void checkout() {
        productOrderService.order(products);
    }

    public void reset() {
        products.clear();
    }
}
