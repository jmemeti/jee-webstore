package com.jee.webstore.beans;

import com.jee.webstore.dao.ProductDao;
import com.jee.webstore.persistence.Product;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;

@Stateful
@SessionScoped
public class ProductService implements Serializable {

    @EJB
    private ProductDao productDao;

    public Product getProduct(long id) {
        return productDao.getProduct(id);
    }

    public List<Product> getAllProducts() {
        return productDao.getAllProducts();
    }
}
