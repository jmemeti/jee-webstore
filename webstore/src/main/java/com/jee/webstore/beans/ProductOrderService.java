package com.jee.webstore.beans;

import com.jee.webstore.common.message.OrderDto;
import com.jee.webstore.persistence.Product;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class ProductOrderService {

	@Resource
	private SessionContext context;

	@Inject
	@JMSConnectionFactory("java:/ConnectionFactory")
	private JMSContext jmsContext;

	@Resource(lookup = "java:/queue/order")
	private Queue target;

	public void order(List<Product> products) {
		List<String> externalProductIds = products.stream()
				.map(Product::getExternalId)
				.collect(Collectors.toList());
		OrderDto orderDto = new OrderDto();
		orderDto.setExternalIds(externalProductIds);
		orderDto.setCustomerUsername(context.getCallerPrincipal().getName());

		jmsContext.createProducer().send(target, orderDto);

		System.err.println("sent jms message: "+orderDto);
	}
}
