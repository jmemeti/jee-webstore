package com.jee.webstore.dao;

import com.jee.webstore.persistence.Product;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Singleton
public class ProductDao {

	@PersistenceContext
	private EntityManager entityManager;

	public Product getProduct(long id) {
		return entityManager.find(Product.class, id);
	}

	public Product getProductByExternalId(String externalId) {
		return entityManager.createQuery("SELECT p FROM Product p WHERE externalId = :externalId", Product.class)
				.setParameter("externalId", externalId).getSingleResult();
	}

	public List<Product> getAllProducts() {
		Query query = entityManager.createNamedQuery("findAllProducts");
		List<Product> products = query.getResultList();
		return products;
	}
}
