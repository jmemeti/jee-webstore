package com.jee.webstore.services;

import com.jee.webstore.beans.ProductService;
import com.jee.webstore.persistence.Product;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@RequestScoped
@Path("/products")
public class ProductRestService {

    @EJB
    private ProductService productService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProducts() {
        List<Product> allProducts = productService.getAllProducts();
        return Response.ok(allProducts).build();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getProduct(@PathParam("id") long id) {
        Product product = productService.getProduct(id);
        return Response.ok(product).build();
    }
}