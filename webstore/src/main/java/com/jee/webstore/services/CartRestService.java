package com.jee.webstore.services;

import com.jee.webstore.beans.CartBean;
import com.jee.webstore.dao.ProductDao;
import com.jee.webstore.persistence.Product;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.io.Serializable;

@SessionScoped
@Path("/cart")
public class CartRestService implements Serializable{

    @EJB
    private CartBean cartBean;
    @EJB
    private ProductDao productDao;

    @POST
    public Response addProduct(@QueryParam("id") long productId) {
        Product product = productDao.getProduct(productId);
        if (product == null) {
            throw new NotFoundException("not found");
        }
        cartBean.addProduct(product);
        return Response.noContent().build();
    }

    @POST
    @Path("/checkout")
    public Response checkout() {
        cartBean.checkout();
        cartBean.reset();
        return Response.noContent().build();
    }
}
