package com.jee.webstore.persistence;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PRODUCTS")
@NamedQueries(@NamedQuery(name = "findAllProducts", query = "select p from Product p order by p.id"))
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(nullable = false, name = "EXT_ID")
	private String externalId;
	@Column(nullable = false, name = "PRODUCT_NAME")
	private String name;
	@Column(nullable = false)
	private double price;
	@Column(nullable = false, name = "NUMBER_OF_AVAILABLE_ITEMS")
	private int numberOfAvailableItems;
	@Column(name = "IMG_URL")
	private String imgUrl;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getNumberOfAvailableItems() {
		return numberOfAvailableItems;
	}

	public void setNumberOfAvailableItems(int numberOfAvailableItems) {
		this.numberOfAvailableItems = numberOfAvailableItems;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
}